console.log("Thanks god it's Friday!")
/*
Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.

3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…


5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.

7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.

9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.

14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/


const getcube = 2 ** 3

let num = getcube;
console.log(`The cube of 2 is ${num}`);



const address = ["258", "Washington Ave NW", "California"];
const [add, city, country, zipCode] = address;

console.log(`I live at ${add} ${city}, ${country} ${zipCode}`);



const animal = {
    weight: "1075 kgs",
    lengt: "20 ft 3 in",
    type: "crocodile"
}

const {weight, lengt, type} = animal;
console.log(`Lolong was a saltwater ${type}. He weighed at ${weight} with a measurement of ${lengt}.`)


const numbers = [1, 2, 3, 4, 5]

numbers.forEach((number) => {
	console.log(`${number}`);
})

let i = 0;
    let reducedArray = numbers.reduce(function(acc,cur){
        (++i);
        return acc + cur;
    });

    console.log(reducedArray);


class Dog {
  constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// Instantiating an object
const myDog = new Dog();

const myNewDog = new Dog("Frankie", "5", "Miniature Dachsmund");
console.log(myNewDog);

